package com.zodiac;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Hello world!
 *
 */
public class AppClient {

    private static Logger logger = LoggerFactory.getLogger(AppClient.class);

    private ClientThread[] threads;

    private AtomicInteger globalCounter = new AtomicInteger();

    AppClient(int threadsNum, ThreadParam threadParam) {
        threads = new ClientThread[threadsNum];

        for (int i = 0; i < threads.length; i++) {
            threads[i] = new ClientThread(threadParam, globalCounter);
        }
    }

    void start()
    {
        for (ClientThread thread1 : threads) {
            thread1.start();
        }

        try {
            for (ClientThread thread : threads) {
                thread.join();
            }
            // At this point all clients are dead with errors.
        } catch (InterruptedException ignored) {
        }
    }

    void cleanUpAndReport()
    {
        // send stop signal to all threads
        for (ClientThread thread : threads) {
            thread.stopClient();
        }
        try {
            for (ClientThread thread : threads) {
                thread.join();
            }
            // At this point all clients are dead with errors.
        } catch (InterruptedException ignored) {
        }
        logger.info("Done. Sent {} messages", globalCounter.get());
    }


    public static void main(String[] args) {

        final AppClient client;

        String propPath = System.getProperty("com.zodiac.properties", "client.properties");

//        try (InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(propPath)) {
        try (InputStream input = new FileInputStream(propPath)) {
//            if (input == null) {
//                throw new Exception("Properties file " + propPath + " is not found in classpath");
//            }

            Properties prop = new Properties();
            prop.load(input);

            int threadsNum = Integer.parseInt(prop.getProperty("threads"));
            ThreadParam threadParam = new ThreadParam(
                    prop.getProperty("host"),
                    Integer.parseInt(prop.getProperty("port")),
                    Integer.parseInt(prop.getProperty("payload")),
                    Integer.parseInt(prop.getProperty("interval")));

            client = new AppClient(threadsNum, threadParam);

            Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    client.cleanUpAndReport();
                }
            });

            try {
                logger.info("Creating {} threads for {}:{}", threadsNum, threadParam.getHost(), threadParam.getPort());
                client.start();
            } catch (Exception e) {
                logger.error(String.format("Error. Total sent {} messages", client.globalCounter.get()), e);
            }


        } catch (Exception e) {
            logger.error("Error", e);
        }
    }
}
