package com.zodiac;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Repeatedly sends random string message to given server and port with interval given in ms.
 */
public class ClientThread extends Thread {

    private static Logger logger = LoggerFactory.getLogger(ClientThread.class);

    private volatile boolean stopFlag = false;
    private ThreadParam threadParam;

    /**
     * Ref to global single counter of sent messages.
     */
    private AtomicInteger globalCounter;

    /**
     * @param threadParam Host,port,interval settings for sender.
     * @param counter Reference to global counter of sent messages.
     */
    public ClientThread(ThreadParam threadParam, AtomicInteger counter) {
        this.threadParam = threadParam;
        this.globalCounter = counter;
    }

    public void stopClient() {
        this.stopFlag = true;
        this.interrupt();
    }

    @Override
    public void run() {
        // Count just for logs
        int localCounter = 0;

        while (!stopFlag) {
            try (Socket socket = new Socket(threadParam.getHost(), threadParam.getPort())) {
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

                writer.write(RandomStringUtils.randomAlphanumeric(threadParam.getPayloadLength()));
                writer.newLine();
                writer.flush();
                int globalCount = globalCounter.incrementAndGet();

                logger.debug("[Thread #{}] sent payload #{} (Total #{})", getId(), localCounter++, globalCount);
            }
            catch (IOException e) {
                logger.error("[Thread #{}] {}", getId(), e.getMessage());
            }

            try {
                Thread.sleep(threadParam.getInterval());
            } catch (InterruptedException ignored) {
                // mark thread as being interrupted
                Thread.currentThread().interrupt();
            }
        }
    }
}
