package com.zodiac;

/**
 * Object parameter for each runner thread.
 */
public class ThreadParam {
    private final String host;
    private final int port;
    private final int payloadLength;
    private final int interval;

    public ThreadParam(String host, int port, int payloadLength, int interval) {
        this.host = host;
        this.port = port;
        this.payloadLength = payloadLength;
        this.interval = interval;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public int getPayloadLength() {
        return payloadLength;
    }

    public int getInterval() {
        return interval;
    }
}
