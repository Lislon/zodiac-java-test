#!/usr/bin/env bash
#      author: lislon
# description: Script for building final zip file with scripts and jars

set -e

BASEPATH=$(realpath $(dirname $0)/../..)
RELEASEDIR="/tmp/release"
BUILDDIR="$RELEASEDIR/client-server"

cd "$BASEPATH"

mvn clean package

rm -rf "$RELEASEDIR"
mkdir -p "$BUILDDIR"

cp ./README.md "$BUILDDIR"
cp -r ./scripts "$BUILDDIR/"

for module in client server; do
    mkdir -p "$BUILDDIR/$module"
    cp ./$module/target/$module-jar-with-dependencies.jar "$BUILDDIR/$module/$module.jar"
    cp ./$module/target/classes/*.properties "$BUILDDIR/$module"
done

cd $RELEASEDIR
zip -r "$RELEASEDIR/client-server.zip" client-server >/dev/null

echo "Build done in $RELEASEDIR"
