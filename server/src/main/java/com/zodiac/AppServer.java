package com.zodiac;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Receives TCP connections from clients, waits till they send a random string and responds with "OK\n" string.
 */
public class AppServer {

    /**
     * Size of queue of TCP connections waiting in pool until thread pick up them.
     */
    private final static int SOCKET_BACKLOG = 50;

    /**
     * Number of worker threads.
     */
    private static final int N_THREADS = 10;

    private static Logger logger = LoggerFactory.getLogger(AppServer.class);

    public static void main(String[] args)
    {
        String propPath = System.getProperty("com.zodiac.properties", "server.properties");

//        try (InputStream input = Thread.currentThread().getContextClassLoader().getResourceAsStream(propPath)) {
//            if (input == null) {
//                throw new Exception("Properties file " + propPath + " is not found in classpath");
//            }
        try (InputStream input = new FileInputStream(propPath)) {

            Properties prop = new Properties();
            prop.load(input);

            String listen = prop.getProperty("listen");
            int port = Integer.parseInt(prop.getProperty("port"));
            int delay = Integer.parseInt(prop.getProperty("delay"));

            ServerSocket serverSocket = new ServerSocket(port, AppServer.SOCKET_BACKLOG, InetAddress.getByName(listen));
            logger.info("Listening for clients on {}:{}...", listen, port);

            ExecutorService threadPool = Executors.newFixedThreadPool(N_THREADS);

            while (!Thread.currentThread().isInterrupted()) {
                Socket socket;
                try {
                    socket = serverSocket.accept();
                    logger.trace("New connection");
                    threadPool.execute(new ServerThread(socket, delay));
                } catch (EOFException e) {
                    logger.warn("Unexpected interrupt");
                }
            }
            logger.info("Done");
        } catch (Exception e) {
            logger.error("Error", e);
        }
    }
}
