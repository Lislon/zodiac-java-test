package com.zodiac;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;

public class ServerThread implements Runnable {

    private static Logger logger = LoggerFactory.getLogger(ServerThread.class);
    private Socket socket;
    private int delay;

    public ServerThread(Socket socket,  int delay) {
        this.socket = socket;
        this.delay = delay;
    }

    @Override
    public void run() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            logger.trace("Wait for payload");
            String payload = reader.readLine();
            logger.trace("Payload received length = {}. Sleeping", payload.length());
            Thread.sleep(delay);
            logger.debug("Sending OK in response");
            writer.write("OK\n");
        } catch (Exception e) {
            logger.warn("Error receiving data", e);
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                logger.warn("Error closing socket", e);
            }
        }
    }
}
